﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryTrigger : MonoBehaviour {

	private ScoreKeeper scoreKeeper;
	private BallController ball;
	private AudioSource sfxVictory;

	void Awake () {

		scoreKeeper = GameObject.Find("ScoreKeeper").GetComponent<ScoreKeeper>();
		ball = GameObject.Find("Ball").GetComponent<BallController>();
		sfxVictory = gameObject.GetComponent<AudioSource>();

	}

	void OnTriggerEnter (Collider other) {

		if (other.gameObject.name == "Ball") {

			if (other.gameObject.transform.position.x > 0) {

				scoreKeeper.player1Score += 1;

			} else {

				scoreKeeper.player2Score += 1;

			}

			ball.ResetBall();

		}

	}

	public void OnCollisionEnter(Collision other) {

		sfxVictory.Play();

	}

}
