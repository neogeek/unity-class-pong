﻿using UnityEngine;

public class PaddleController : MonoBehaviour {

	public string keyUp = "w";
	public string keyDown = "s";

	public bool isAI = false;

	private GameObject ball;
	private int speed = 20;
	private int computerSpeed = 10;

	void Awake () {

		ball = GameObject.Find("Ball").gameObject;

	}

	void Update () {

		if (isAI) {

			gameObject.transform.position = new Vector3(
				gameObject.transform.position.x,
				Mathf.Lerp(gameObject.transform.position.y, ball.transform.position.y, computerSpeed * Time.deltaTime),
				gameObject.transform.position.z
			);

		} else {

			if (Input.GetKey(keyUp)) {

				gameObject.transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));

			} else if (Input.GetKey(keyDown)) {

				gameObject.transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));

			}

		}

		gameObject.transform.position = new Vector3(
			gameObject.transform.position.x,
			Mathf.Clamp(gameObject.transform.position.y, -12, 12),
			gameObject.transform.position.z
		);

	}

}
