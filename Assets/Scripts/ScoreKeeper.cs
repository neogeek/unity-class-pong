﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour {

	public int player1Score = 0;
	public int player2Score = 0;

	void Start () {

		Reset();

	}

	void Reset () {

		player1Score = 0;
		player2Score = 0;

	}

	void OnGUI () {

		GUI.Box(new Rect(10, 10, 90, 30), "Player 1: " + player1Score);
		GUI.Box(new Rect(Screen.width - 100, 10, 90, 30), "Player 2: " + player2Score);

	}

}
