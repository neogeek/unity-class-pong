﻿using UnityEngine;

public class BallController : MonoBehaviour {

	private Rigidbody rb;
	private ScoreKeeper scoreKeeper;
	private AudioSource sfxBounce;

	void Awake () {

		rb = gameObject.GetComponent<Rigidbody>();

		scoreKeeper = GameObject.Find("ScoreKeeper").GetComponent<ScoreKeeper>();

		sfxBounce = gameObject.GetComponent<AudioSource>();

	}

	void Start () {

		Invoke("ResetBall", 3.0f);

	}

	public void ResetBall () {

		gameObject.transform.position = Vector3.zero;

		rb.velocity = Vector3.zero;

		rb.AddForce(new Vector3(Random.Range(6, 8), Random.Range(-10, 10), 0));

	}

	public void OnCollisionEnter(Collision other) {

		sfxBounce.Play();

	}

}
